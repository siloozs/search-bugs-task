import React , {Component} from 'react';
import './App.css';
import { Container , Divider  } from 'semantic-ui-react'; 
import SearchBar from './SearchBar';
import List from './List';
import axios from 'axios';
import Message from './Message';

export default class App extends Component {
 state = {data:[] , error: false }
  onSearchedInput = (value) =>{
    axios.get('https://test-api.techsee.me/api/ex/' + value)
      .then(result =>{
          if(result.data && result.data.length){
                this.setState({data: result.data});
          }else if(result.status !== 200){
              this.setState({error: true});    
          }
      })
      .catch(err =>{
        this.setState({error: true});
      });
  }
  
  render() {
    return (
      <div className="App">
      <Container>
          <Divider horizontal>Search of testers</Divider>
          <SearchBar onSearchedInput={this.onSearchedInput}/>
           {this.state.error && <Message text="Temporary error occurred, please try again later" />}
          <List data={this.state.data}/>
      </Container>
    </div>
    )
  }
}








