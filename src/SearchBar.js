import React, { Component } from 'react'
import { Input , Button , Icon} from 'semantic-ui-react'; 


export default class SearchBar extends Component {
 state = { inputValue: '' , disabledBtn: true }
  
 updateInput = e =>{
      this.setState({ inputValue: e.target.value , disabledBtn: e.target.value.length < 2 });
 }
 
 searchBug = () =>{
  this.props.onSearchedInput(this.state.inputValue);
 }

  render() {
    return (
      <div>
          <div className="ui action input">
          <input className="search-input" minLength="2" maxLength="12" type="text" placeholder="Enter the tester name..." value={this.state.inputValue} onChange={this.updateInput} />
          <button className="ui button primary " disabled={this.state.disabledBtn} onClick={this.searchBug}>
              <i aria-hidden="true" className="search icon"></i>
              Fetch
          </button>
      </div>
          
      </div>
    )
  }
}
