import React from 'react'

export default function Message({text}) {
  return (
    <div className="ui error message">
        {text}
      </div>
  )
}
