
import { Table } from 'semantic-ui-react';
import React, { Component } from 'react'
// import data from './data.json';

export default class List extends Component {
  state = { sortedBy: 'first' }

  sortBySet = e =>{
        let sortedBy = 'first';
        if(e.target.classList.contains('tiny-button')){
          sortedBy  = e.target.dataset.by;
        }else if(e.target.parentElement.classList.contains('tiny-button')){
          sortedBy  = e.target.parentElement.dataset.by;
        }
        this.setState({sortedBy});
  }

  sort = (a,b) =>{
    switch(this.state.sortedBy){
      case 'first':
        return a.firstName.toLowerCase() > b.firstName.toLowerCase() ? 1 : -1;
      case 'last':
        return a.lastName.toLowerCase() > b.lastName.toLowerCase() ? 1 : -1;
      case 'county':
        return a.country.toLowerCase() > b.country.toLowerCase() ? 1 : -1;
      default:
        return a.firstName.toLowerCase() > b.firstName.toLowerCase() ? 1 : -1;
    }
  }


  render() {

    if(this.props.data.length < 1){
      return null;
    }

    return (
    this.props.data && this.props.data.length && ( 
     <Table celled padded>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>First Name
          <button className="ui icon button tiny-button" data-by="first" onClick={this.sortBySet}><i aria-hidden="true" className="caret down icon"></i></button>
          </Table.HeaderCell>
          <Table.HeaderCell className="wide-row">Last Name
          <button className="ui icon button tiny-button" data-by="last" onClick={this.sortBySet}><i aria-hidden="true" className="caret down icon"></i></button>
          </Table.HeaderCell>
          <Table.HeaderCell className="wide-row">Country
          <button className="ui icon button tiny-button" data-by="county" onClick={this.sortBySet}><i aria-hidden="true" className="caret down icon"></i></button>
          </Table.HeaderCell>
          <Table.HeaderCell>Bugs</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {
          this.props.data.sort(this.sort).map((item , idx) =>(
          <Table.Row key={idx}>
            <Table.Cell>
              {item.firstName}
             </Table.Cell>
             <Table.Cell>
              {item.lastName}
             </Table.Cell>
             <Table.Cell>
              {item.country}
             </Table.Cell>
             <Table.Cell>
              {item.bugs.map(bug => bug.title).join(',')}
             </Table.Cell>
          </Table.Row> 
          ))
        }
        
    </Table.Body>    
      </Table>) ) 
   
  }
}
